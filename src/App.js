import {connect, StringCodec, credsAuthenticator} from "nats.ws";
import React from "react";

async function natsDemo() {
  const creds = `-----BEGIN NATS USER JWT-----
eyJ0eXAiOiJKV1QiLCJhbGciOiJlZDI1NTE5LW5rZXkifQ.eyJqdGkiOiJXNk82SUJPWEI3Uk43WUhONkpSTVdSREtJWEpZQlkzT0FYNEhHVFQ0NVNKVEhHWE82TFNRIiwiaWF0IjoxNjQ4ODk5NzEwLCJpc3MiOiJBREpNS1FPSlY2S1hZS1dLUlU0VVZER0hOWFU3NUdYSTIzTkFYN0lONVBQM01ZUE5PUVRSSkhEVSIsIm5hbWUiOiJ0ZXN0Iiwic3ViIjoiVURFN0JZUlkzT0tXV1I0QURQWDRHSDdJNUgyQzJUSFpISlM0NUJNRVhHNkNVNEg1SkNHVlJWT1EiLCJuYXRzIjp7InB1YiI6e30sInN1YiI6e30sInN1YnMiOi0xLCJkYXRhIjotMSwicGF5bG9hZCI6LTEsInR5cGUiOiJ1c2VyIiwidmVyc2lvbiI6Mn19.u2m69Wkjyw0pG1h5wc1BvJ3tdUHZRylk4BUArGy7PRmbRj2fFV1RU0OMDtMQnYZTn0B3P3Tmh68W_AmR7oRjDQ
------END NATS USER JWT------

************************* IMPORTANT *************************
NKEY Seed printed below can be used to sign and prove identity.
NKEYs are sensitive and should be treated as secrets.

-----BEGIN USER NKEY SEED-----
SUAPFEIVPQU2LU3BVGKGXPXJXN3FJAIQS7LEVRWKR2JSQZM7CIW5KSENM4
------END USER NKEY SEED------

*************************************************************`;

  const nc = await connect({
    servers: "tls://connect.ngs.global:4222",
    authenticator: credsAuthenticator(new TextEncoder().encode(creds)),
  });

  const sc = StringCodec();
  const sub = nc.subscribe("hello");
  (async () => {
    for await (const m of sub) {
      console.log(`[${sub.getProcessed()}]: ${sc.decode(m.data)}`);
    }
    console.log("subscription closed");
  })();

  nc.publish("hello", sc.encode("Hello from JS!"));
}

function App() {
  natsDemo().then();
  return (
    <div className="App">
      DEMO
    </div>
  );
}

export default App;
